package main

import (
	"fmt"
	ll "gitlab.com/dparker/go-generics/linkedlist"
)

func main() {
	nodeA := ll.NewNode(1)
	nodeA.InsertAfter(ll.NewNode(2)).InsertAfter(ll.NewNode(3)).InsertAfter(ll.NewNode(4))

	nodeA.PrintList()

	fmt.Println(nodeA)
	fmt.Println(nodeA.Next)
	fmt.Println(nodeA.Previous)

	nodeA.Next.Next.Remove()

	nodeA.PrintList()

	fmt.Println(nodeA.Tail())

	nodeA.Tail().InsertBefore(ll.NewNode(10))
	nodeA.PrintList()

	nodeA.Tail().InsertAfter(ll.NewNode(11)).Tail()
	nodeA.PrintList()

	nodeA.Head().InsertBefore(ll.NewNode(-1)).Head()
	nodeA.PrintList()

	stringList := ll.NewNode("A").InsertAfter(ll.NewNode("B")).InsertAfter(ll.NewNode("C")).Head()
	stringList.PrintList()
}
