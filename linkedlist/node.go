package linkedlist

import (
	"fmt"
	"reflect"
)

type Node[T any] struct {
	Value    T
	Next     *Node[T]
	Previous *Node[T]
}

func (n *Node[T]) InsertAfter(next *Node[T]) *Node[T] {
	// n -> Next -> oldNext
	oldNext := n.Next
	n.Next = next
	next.Next = oldNext
	next.Previous = n
	oldNext.Previous = next
	return next
}

func (n *Node[T]) InsertBefore(previous *Node[T]) *Node[T] {
	// oldPrevious -> Previous -> n
	oldPrevious := n.Previous
	n.Previous = previous
	previous.Previous = oldPrevious
	previous.Next = n
	oldPrevious.Next = previous
	return previous
}

func (n *Node[T]) Remove() {
	if n.Previous != Sentinel[T]() {
		n.Previous.Next = n.Next
	}

	if n.Next != Sentinel[T]() {
		n.Next.Previous = n.Previous
	}
}

func (n *Node[T]) Head() *Node[T] {
	current := n
	for current.Previous != Sentinel[T]() {
		current = current.Previous
	}

	return current
}

func (n *Node[T]) Tail() *Node[T] {
	current := n
	for current.Next != Sentinel[T]() {
		current = current.Next
	}

	return current
}

func (n *Node[T]) String() string {
	return fmt.Sprintf("%v", n.Value)
}

func (n *Node[T]) PrintList() {
	current := n.Head()
	if current.Next != Sentinel[T]() {
		fmt.Printf("%v, ", current)
	} else {
		fmt.Print(current)
	}

	for current.Next != Sentinel[T]() {
		current = current.Next
		if current.Next != Sentinel[T]() {
			fmt.Printf("%v, ", current)
		} else {
			fmt.Print(current)
		}
	}
	fmt.Printf("\n")
}

func NewNode[T any](value T) *Node[T] {
	return &Node[T]{
		Value:    value,
		Previous: Sentinel[T](),
		Next:     Sentinel[T](),
	}
}

var sentinels []any

func Sentinel[T any]() *Node[T] {
	var ourSentinel *Node[T]

	for _, sentinel := range sentinels {
		if reflect.TypeOf(sentinel) == reflect.TypeOf(ourSentinel) {
			ourSentinel = sentinel.(*Node[T])
		}
	}

	if ourSentinel != nil {
		return ourSentinel
	}

	var nilVal T

	null := Node[T]{
		Value: nilVal,
	}

	null.Next = &null
	null.Previous = &null

	ourSentinel = &null

	sentinels = append(sentinels, ourSentinel)

	return &null
}
